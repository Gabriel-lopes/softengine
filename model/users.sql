-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3308
-- Généré le :  mar. 18 mai 2021 à 12:13
-- Version du serveur :  8.0.18
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `projetbts`
--

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de l''utilisateur',
  `role` int(11) NOT NULL DEFAULT '0' COMMENT '0 pour utilisateur lambda et 1 pour un administrateur',
  `pseudo` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'Pseudonyme de l''utilisateur',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'Adresse email de l''utilisateur',
  `mdp` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'Mot de passe de l''utilisateur',
  `date` date NOT NULL COMMENT 'Date d''inscription de l''utilisateur',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Table de l''utilisateur';

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `role`, `pseudo`, `email`, `mdp`, `date`) VALUES
(2, 0, 'test', 'test@gmail.com', 'ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff', '2021-04-26'),
(3, 1, 'Admin', 'admin@gmail.com', 'c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec', '2021-04-28'),
(17, 0, 'new', 'new@gmail.com', 'aa54def9e0bb11c1ebbfc97a9ee63af9e95c4fdf1d032b1ddcc0f21661f748651d2b2b8fb94e9ae041780554db29815daa1c0fe991ddae54eff0c4c28cd9d20c', '2021-05-14'),
(24, 0, 'new5', 'new5@gmail.com', 'aa54def9e0bb11c1ebbfc97a9ee63af9e95c4fdf1d032b1ddcc0f21661f748651d2b2b8fb94e9ae041780554db29815daa1c0fe991ddae54eff0c4c28cd9d20c', '2021-05-17'),
(16, 0, 'essai', 'essai@gmail.com', '314730a4b5bd78a34ddde888f23eb95ce66be736134da9f21b06e3229acab073d2a7ba3f4080e63d4054dabdbffedef34c6cd13598040211486a6a05debd24b5', '2021-05-14'),
(23, 0, 'new3', 'new3@gmail.com', '27ac483110ea90341e1f2e9381fc8b81b703bf7907b00d0d884a97eccd59b2c5a7eb86a60847ce666f1e5429141cc2878488d2585f02b5d8eed215e3980a327a', '2021-05-14');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
