<?php
require_once('config/connex.php');

//Fonction qui permet d'ajouter un utilisateur
//La variable $mdp2 sert à confirmer le mot de passe
function addUser($pseudo, $mdp, $mdp2, $email) {
    try {
        $check = connect()->prepare("SELECT * FROM users WHERE email = ?");
        $check->execute(array($email));
        $result = $check->fetch(PDO::FETCH_OBJ);
		if($mdp == $mdp2) { //rajouté pour confirmer le mot de passe
			if ($result == false) { //S'il n'existe pas déjà
				$hashpwd = hash('sha512', $mdp);
				$req = connect()->prepare("INSERT INTO users(pseudo, mdp, email, role, date) VALUES (?, ?, ?, 0, NOW())"); //Le 0 car ce n'est pas un admin
				$req->execute(array($pseudo, $hashpwd, $email));
				$req->closeCursor();
				return true;
			} else{
				return false;
			}
		} //fin de la confirmation du mot de passe
	}
	catch (PDOException $e) {
		echo "Le compte n'a pas été créé : ".$e->getMessage();
	}
}

//Fonction qui permet de se connecter
function login($email, $mdp) {
    $hashpwd = hash('sha512', $mdp);

    $req = connect()->prepare("SELECT id, pseudo, role, email, date, mdp FROM users WHERE email = ? AND mdp = ?");
    $req->execute(array($email, $hashpwd));
	//var_dump($req);
    $user = $req->fetch(PDO::FETCH_OBJ);
    if ($user) {
		$_SESSION['id'] = $user->id;
        $_SESSION['pseudo'] = $user->pseudo;
        $_SESSION['role'] = $user->role;
		$_SESSION['email'] = $user->email;
		$_SESSION['date'] = $user->date;
		$_SESSION['mdp'] = $user->mdp;
        $_SESSION['session'] = "";
		//var_dump($_SESSION);
		return $_SESSION;
    }
}

function delete($mdp, $cmdp) {
	if($mdp == $cmdp) {
		$hashpwd1 = hash('sha512', $mdp);
		$hashpwd2 = hash('sha512', $cmdp);
		if($hashpwd1 == $_SESSION['mdp']) {
			$_SESSION['del'] = "";
			return $_SESSION;
		}
	}
}

//Fonction pour créer des topics
function addTopic($titre, $contenu, $auteur) {
	try {
		$req = connect()->prepare("INSERT INTO topic(titre, contenu, auteur, date) VALUES (?, ?, ?, NOW())");
		$req->execute(array($titre, $contenu, $auteur));
		$req->closeCursor();
	}
	catch (PDOException $e) {
		echo "Le topic n'a pas été créé : ".$e->getMessage();
	}
}

//Fonction qui récupère tous les topics et les affiches dans index
function getTopics(){
	try{
		$req = connect()->prepare('SELECT id, titre, contenu, auteur, date FROM topic ORDER BY id'); //On prepare la prise des éléments select 
		$req->execute(); //On apllique
		$data = $req->fetchAll(PDO::FETCH_OBJ); //la variable data récupère les attributs (id, title ...) de l'objet
		return $data; //On retourne cette variable
		$req->closeCursor(); // libère la connexion au serveur, permettant d'effectuer de nouvelles requetes SQL
	}
	catch(PDOException $e) {
		echo 'Petit Problème : '.$e->getMessage();
	}
}

//Fonction qui récupère un topic et l'affiche dans une page
function getTopic($id) {
	try{
		$req = connect()->prepare('SELECT * FROM topic WHERE id = ?'); //prepare de tous les éléments de ka table article dont l'id est en parametre
		$req->execute(array($id)); //array quand on prend tous les éléments (fetchAll) 
		
		if($req->rowCount() == 1) { // s'il existe qu'un seul objet sous cet id
			$data = $req->fetch(PDO::FETCH_OBJ); //la variable data récupère les attributs (* = tous) de l'objet
			return $data; //On retourne cette variable
		} else { //sinon
			header('Location:index.php'); //redirection vers la page index (accueil)
		} //fin du sinon
		
		$req->closeCursor(); // libère la connexion au serveur, permettant d'effectuer de nouvelles requetes SQL
	}
	catch(PDOException $e) {
		return 'Petit Problème : '.$e->getMessage();
	}
}

//Fonction qui permet à un utilisateur d'ajouter un commentaire
function addComment($id_topic, $auteur, $contenu) {
	try {
		$req = connect()->prepare('INSERT INTO message (id_topic, auteur, contenu, date) VALUES (?, ?, ?, NOW())');
		$req->execute(array($id_topic, $auteur, $contenu));
		$req->closeCursor();
	}
	catch(PDOException $e) {
		echo 'Le commentaire n\'a pas pu être enregistré : '.$e->getMessage();
	}
}

//recuperer les commentaires
function getComment($id_topic){
  try{
		$req= connect()->prepare('SELECT * FROM message WHERE id_topic = ?');
		$req->execute(array($id_topic));
		$data = $req->fetchALL(PDO::FETCH_OBJ);
		return $data;
		$req->closeCursor();
	} 
	catch (PDOException $e){
		echo 'Commentaire inexistant : '.$e->getMessage();
	}
}

//Fonction qui permet de modifier le pseudo d'un utilisateur
function modifPseudo($pseudo, $id){
	$req= connect()->prepare('UPDATE users SET pseudo = "'.$pseudo.'" WHERE id = "'.$id.'"');
	$req->execute(array($pseudo));
	$req->closeCursor();
}

//Fonction qui permet de modifier l'email d'un utilisateur
function modifEmail($email, $id){
	$req= connect()->prepare('UPDATE users SET email = "'.$email.'" WHERE id = "'.$id.'"');
	$req->execute(array($email));
	$req->closeCursor();
}

?>