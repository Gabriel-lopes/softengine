<?php
session_start();
require_once('functions.php');
?>

<DOCTYPE html>

<head>
	<meta charset="utf-8">
	<title>Profil - <?php echo $_SESSION['pseudo']; ?></title>
	<link rel="stylesheet" href="css/style.css">
</head>
	
<body>
	<?php 
	//Si cette session n'existe pas vous avez un message d'erreur :
	if(!isset ($_SESSION['session'])) { ?>
		<div class="non" align="center">
			<br><br><br>
			<a href="index.php">Accueil</a><br>
			<p>Vous ne pouvez pas consulter votre profil car vous n'êtes pas <a href="login.php">connecté</a> !</p>
		</div>
	<?php } else { ?>
	
	<br><br>
	<pre>   <a href="index.php">Accueil</a> </pre>
	<br><br><br>
	<div align="center">
	<h1>Profil de <?php echo $_SESSION['pseudo']; ?></h1>
	<br>

	<?php if($_SESSION['role'] == 1) {
		echo "<p>Vous êtes Administrateur</p>";
		echo '<br><br>';
	} ?>

	<table>
		<tr><td>Pseudonyme : <?php echo $_SESSION['pseudo']; ?> </td></tr>
		<tr><td>E-mail : <?php echo $_SESSION['email']; ?> </td></tr>
		<tr><td>Membre depuis : <?php echo $_SESSION['date']; ?> </td></tr>
	</table>
	
	<br>
	<a href="edit.php">Modifier votre profil</a>
	<br><br>
	<a href="confirm.php">Supprimer votre compte</a>
	<br><br>
	<a href="logout.php">Déconnexion</a>
	
	</div>
	
	<?php } ?> 
	
</body>
	
</html>