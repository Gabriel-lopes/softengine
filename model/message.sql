-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3308
-- Généré le :  mar. 18 mai 2021 à 12:13
-- Version du serveur :  8.0.18
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `projetbts`
--

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

DROP TABLE IF EXISTS `message`;
CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id du message',
  `id_topic` int(11) NOT NULL COMMENT 'id du topic auquel le message appartient',
  `auteur` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'Auteur du message',
  `contenu` varchar(2000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'Contenu du message (le message quoi)',
  `date` date NOT NULL COMMENT 'Date de publication du message',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Table du message';

--
-- Déchargement des données de la table `message`
--

INSERT INTO `message` (`id`, `id_topic`, `auteur`, `contenu`, `date`) VALUES
(1, 1, 'Admin', 'Je test d\'upload le premier commentaire sur le premier topic', '2021-05-16'),
(2, 3, 'Admin', 'C\'est simple je vais t\'envoyer un lien dans les prochaines heures, reste connecté !', '2021-05-16'),
(3, 3, 'Admin', 'C\'est simple je vais t\'envoyer un lien dans les prochaines heures, reste connecté !', '2021-05-16'),
(4, 1, 'Admin', 'test', '2021-05-16'),
(5, 1, 'Admin', 'test', '2021-05-16'),
(6, 4, 'Admin', 'premier commentaire du quatrième topic', '2021-05-16');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
