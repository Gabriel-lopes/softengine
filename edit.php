<?php 
session_start();
require_once("functions.php");

if(!empty($_POST)) {

	$id = $_SESSION['id'];
	$mdp = $_SESSION['mdp'];
	//$hashpassword = hash('sha512', $mdp);
	$newpseudo = strip_tags($_POST['newpseudo']);
	$newemail = strip_tags($_POST['newemail']);
	$newmdp1 = strip_tags($_POST['newmdp1']);
	$newmdp2 = strip_tags($_POST['newmdp2']);
	$newmdp3 = hash('sha512', $newmdp1);
	$newmdp4 = hash('sha512', $newmdp2);
	
	//Condition pour tous changer 
	if(!empty($newmdp1) AND !empty($newmdp2) AND $newmdp3 == $newmdp4 AND $newmdp3 == $mdp) {
			
	//Condition pour changer le pseudo
		if(!empty($newpseudo)) {
			try {
				$change = modifPseudo($newpseudo, $id);
				//header("Location:profil.php");
				if ($change == false) {
					$return = "Le pseudo n'a pas pu être modifier !";
				}
			} catch (Exception $e) {
				echo "problème avec la méthode addUser : ".$e->getMessage();
			}
			unset($newpseudo);
		}
	
		//Condition pour changer l'email
		if(!empty($newemail)) {
			try {
				$change = modifEmail($newemail, $id);
				//header("Location:profil.php");
				if ($change == false) {
					$return = "L'adresse e-mail n'a pas pu être modifier !";
				}
			} catch (Exception $e) {
				echo "problème avec la méthode addUser : ".$e->getMessage();
			}
			unset($newemail);
		}
		header("Location:profil.php");
	} else {
		echo "<p>Vous n'avez pas bien confirmer votre mot de passe ! </p>";
		echo "<p>Les changements n'ont pas été enregistrés.<p>";
	}
	unset($mdp);
	unset($newmdp1);
	unset($newmdp2);
}
?>

<!DOCTYPE html>

<head>
	<meta charset="utf-8">
	<title>Edition du Profil</title>
	<link rel="stylesheet" href="css/style_form.css">
</head>

<body>

	<pre>   <a href="profil.php">Retour</a>     <a href="index.php">Accueil</a> </pre>
	<br><br><br>
	
	<!-- Formulaire de modifications de ses propres données : -->
	<br><br><br>
	<form class="box" method="POST" action="#">
		<h1>Modifications</h1>
		<p>Lorsque vos changements auront été complétés, il sera impératif de se reconnecter pour en apercevoir les effets. Ainsi retenez bien vos informations modifiées.</p><br>
		
		<input type="text" name="newpseudo" placeholder="Nouveau Pseudonyme">
		
		<input type="text" name="newemail" placeholder="Nouvelle E-mail"> <br>
		
		<p>Pour valider vos changements entrer votre mot de passe, puis confirmez-le.</p><br>
		<input type="password" name="newmdp1" placeholder="Mot de passe actuel">
		
		<input type="password" name="newmdp2" placeholder="Confirmation du mot de passe">
		
		<br>
		<input class="btn menu"type="submit" name="modification" value="Changer vos informations">
	</form>
	</div>
	
</body>

</html>