-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3308
-- Généré le :  mar. 18 mai 2021 à 12:13
-- Version du serveur :  8.0.18
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `projetbts`
--

-- --------------------------------------------------------

--
-- Structure de la table `topic`
--

DROP TABLE IF EXISTS `topic`;
CREATE TABLE IF NOT EXISTS `topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id du topic',
  `titre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'Titre du topic',
  `contenu` varchar(1000) COLLATE utf8_unicode_ci NOT NULL COMMENT 'contenu du topic',
  `auteur` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'Auteur du titre (user à son origine)',
  `date` date NOT NULL COMMENT 'Date de la création du topic',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Table du topic';

--
-- Déchargement des données de la table `topic`
--

INSERT INTO `topic` (`id`, `titre`, `contenu`, `auteur`, `date`) VALUES
(1, 'Premier topic Test', 'Contenu du premier topic qui servira de test', 'Admin', '2021-05-12'),
(2, 'Deuxième topic', 'Nouveau test ', 'Admin', '2021-05-13'),
(3, 'Vérifier que deux tableaux de tableaux contiennent les mêmes éléments (boolean)', 'Voici l\'un des topic que j\'essai de créer !', 'admin@gmail.com', '2021-05-15'),
(4, 'Quatrième topic deja ', 'Voici l\'un des topic que j\'essai de créer ! normalement on voit mon pseudo à coter pour prouver que ce topic est de moi', 'Admin', '2021-05-15'),
(5, 'Cinquième topic', 'Cinquième topic créer j\'espère qu\'il s\'affiche', 'essai', '2021-05-15'),
(6, 'Sixième topic', 'Voici l\'un des topic que j\'essai de créer !', 'Admin', '2021-05-18');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
