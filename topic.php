<?php
session_start();
require_once("functions.php");

if(!isset($_GET['id']) || !is_numeric($_GET['id'])) { //condition pour l'id du topic
	header('Location:index.php'); //redirection
} else { //sinon (et c'est la que la fonction commence)
	extract($_GET);
	$id = strip_tags($_GET['id']);
	require_once('functions.php');
	
	if(isset($_SESSION['session'])) {
		$auteur = $_SESSION['pseudo'];
	}
	$topic = getTopic($id); //on stock dans une nouvelle variable topic la fonction getTopic (functions.php)

	//Pour ajouter un commentaire (message)
	if(!empty($_POST)) {
		$errors = array(); //On définit une variable pour stocker les erreurs (il s'agit d'un tableau)
		
		$contenu = strip_tags($_POST['contenu']);
		
		if(empty($contenu)) { //Si le commentaire est vide
			array_push($errors, 'Commentaire manquant'); //On ajoute une erreur dans le tableau $errors
		} //fin de la condition sur le commentaire
		
		if(count($errors) == 0) { //Condition : s'il n'y a pas d'erreur on peut :
			try {
				$contenu = addComment($id, $auteur, $contenu); //ajouter un commentaire avec la methode addComment (config/fonction.php)
				$success = "votre commentaire est publié"; //Définir une variable pour afficher un message plus tard
				header("Location:redirection.php");
			} //FIN DE LA CONDITION 's'il n'y a pas d'erreur'
			catch(PDOException $e) {
				echo 'Le commentaire n\'a pas pu être créé : '.$e->getMessage();
			}
			
			//Destruction de variable, pour qu'on puisse potentiellement les réutiliser plus tard
			unset($contenu); //Destruction de la variable $contenu
			
		}
	}
	$topic = getTopic($id);
	$contenu = getComment($id);

}
?>

<!DOCTYPE html>

<head>
	<meta charset="utf-8">
	<title><?php echo $topic->titre ?></title>
</head>


<body>

	<a href="index.php">Accueil</a>
	<br><br>
	<h2><?php echo $topic->titre ?></h2>
	<time> <?php echo "Topic poster par ".$topic->auteur." le ".$topic->date ?></time><br>
	<h3><?php echo $topic->contenu ?></h3>
	<br><br><br>
	
	<h3>Commentaires</h3>
	<?php
	if(isset($contenu) && !empty($contenu)) {
		foreach($contenu as $com){
			echo '<p>'.$com->auteur.' a écrit: </p>';
			echo '<p>'.$com->contenu.'</p>';
			echo '<time>'.$com->date.'</time><br><hr>';
		}
	}
	
	if(isset($_SESSION['session'])) {
	?>
	
	<br><br><br>
	<h3> Laisser un commentaire </h3>
	
	<form action="topic.php?id=<?= $topic->id?>" method="post">
		
		<label for="contenu">Commentaire :</label><br>
		<textarea name="contenu" id="contenu" cols="60" row="10"></textarea><br><br>
		
		<button type="submit">Envoyer</button>
	</form>
	
	<?php
	}
	
	if(isset($success) && !empty($success)) {
		echo $success;
	}
	
	if(!empty($errors)) {
		foreach($errors as $error) {
			echo '<p>'.$error.'</p>';
		}
	}
	?>
	
</body>

</html>