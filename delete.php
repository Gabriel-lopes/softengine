<?php
	session_start();
	require_once('config/connex.php');
	$id = $_SESSION['id'];
	$req = connect()->prepare("DELETE FROM users WHERE id = ?")->execute( array($id) );
	
	$redirect_url = "logout.php";
	header("Location: {$redirect_url}");
	exit;
 ?>