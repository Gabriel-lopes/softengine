<?php 
session_start();
require_once("functions.php");

if(!empty($_POST)) {
	$errors = array();

	$titre = strip_tags($_POST['titre']);
	$contenu = strip_tags($_POST['contenu']);
	$auteur = $_SESSION['pseudo'];

	if(empty($titre)) {
		array_push($errors, 'Il faut un titre à votre topic');
	}
	
	if(empty($contenu)) {
		array_push($errors, 'Il vous faut un contenu pour votre topic');
	}

	if (count($errors) == 0){
		try {
			$topic = addTopic($titre, $contenu, $auteur);
			header("Location:index.php");
			if ($topic == false) {
				$return = "Le topic n'a pas pu être créé, veuillez réessayer ultérieurement";
			}
		} catch (Exception $e) {
			echo "problème avec la méthode addUser : ".$e->getMessage();
		}
		unset($titre);
		unset($contenu);
		unset($auteur);
	}
}
?>

<!DOCTYPE html>

<head>
	<meta charset="utf-8">
	<title>Topic</title>
	<link rel="stylesheet" href="css/style_form2.css">
</head>

<body>
	
	<?php if(!isset ($_SESSION['session'])) { ?>
		<div class="non" align="center">
			<br><br><br>
			<a href="index.php">Accueil</a><br>
			<p>Vous ne pouvez pas créer de topic si vous n'êtes pas <a href="login.php">connecté</a> !</p>
		</div>
	<?php 
	} else {
	?>
	
	<?php
	//Si il y'a des erreurs, il les affiches
	if (!empty($errors)) {
		foreach ($errors as $error){
			echo '<p>'.$error.'</p>';
		}
	} ?>
	
	<pre>   <a href="index.php">Accueil</a> </pre>
	<br><br>
	
	<form class="box" action="create.php" method="post">
		<h1>Créer votre topic</h1>
	
		<input type="text" name="titre" id="titre" placeholder="Titre"><br>

		<input type="text" name="contenu" id="contenu" placeholder="Problème" size="100" style="height:80px;"><br>

		<input type="submit" name="" value="Créer le Topic">	
	</form>
	
</body>

<?php 
}
?>