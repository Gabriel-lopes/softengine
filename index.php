<?php 
session_start();
require_once('functions.php');

$topics = getTopics();
?>

<DOCTYPE html>

<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
	<meta charset="utf-8">
	<title>SoftEngine</title>
	<h3>Le forum qui règle vos problèmes informatiques</h3>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="css/style2.css">

</head>

<body>

<header>
	<div class="container">
      <nav class="navbar navbar-expand navbar-dark bg-dark">
        <a class="navbar-brand" href="index.php">SoftEngine</a>
        <ul class="navbar-nav">
		  <li class="nav-item"><a class="nav-link " href="profil.php">Profil</a></li>
		  <li class="nav-item"><a class="nav-link " href="create.php">Topics</a></li>
		   <?php if(isset($_SESSION['session']) AND $_SESSION['role'] == 1) { ?>
		  <li class="nav-item"><a class="nav-link " href="admin/admin.php">Support</a></li>
		   <?php } ?>
		  <?php if(!isset($_SESSION['session'])) { ?>
		  <li class="nav-item"><a class="nav-link " href="login.php">Se connecter</a></li>
		  <li class="nav-item"><a class="nav-link " href="register.php">S'inscire</a></li>
		  <?php } ?>
		  <?php if(isset($_SESSION['session'])) { ?>
		  <li class="nav-item"><a class="nav-link " href="logout.php">Se déconnecter</a></li>
		  <?php } ?>
		 </ul>
      </nav>
    </div>
</header>

<div class="accueil" align="center">
<h1>Topics</h1>
<br><br>
<table>
	<thead>
	</thead>
	
	<tbody>
		<tr>
			<td>Sujet</td>
			<td>Auteur</td>
			<td>Date</td>
		</tr>
		<tr>
		<td></td>
		</tr>
		<tr>
			<?php foreach($topics as $topic) { //boucle pour passer dans la table topic et afficher tous les topics (topics étant le return de la fonction getTopics() de (functions.php)
			echo '<td><a href ="topic.php?id='.$topic->id.'">'.$topic->titre.'</a></td>'; //Affiche le titre du topic
			echo '<td><time>'.$topic->auteur.'</time></td>'; //affiche la date du topic
			echo '<td><time>'.$topic->date.'</time></td>';
			echo '</tr>';
			} ?>
	</tbody>
	
</table>
</div>
	
</body>

</html>