<?php

require_once('functions.php');

if(!empty($_POST)) {
	$errors = array();
	
	$pseudo = strip_tags($_POST['pseudo']);
	$mdp = strip_tags($_POST['mdp']);
	$mdp2 = strip_tags($_POST['mdp2']); //rajout récent pour la conf du mdp
	$email = strip_tags($_POST['email']);
	
	if(empty($pseudo)) {
		array_push($errors, 'Pseudonymeseudo manquant !');
	}
	
	if(empty($email)) {
		array_push($errors, 'E-mail manquant !');
	}
	
	if(empty($mdp)) {
		array_push($errors, 'Mot de passe manquant !');
	}
	
	//rajout récent -> pour indiquer une erreur lors de la confirmation le mot de passe 
	if($mdp != $mdp2) {
		array_push($errors, 'Votre mot de passe a mal été confirmé !');
	}
	
	if (count($errors) == 0){
        try {
            $user = addUser($pseudo, $mdp, $mdp2, $email);
            if ($user == false) {
                $return = "Le compte n'a pas pu être créé, veullez saisir un autre email";
            }
		} catch (Exception $e) {
            echo "problème avec la méthode addUser : ".$e->getMessage();
        }
        unset($pseudo);
        unset($mdp);
		unset($mdp2);
        unset($email);
    }
}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Créer un compte</title>
	<link rel="stylesheet" href="css/style_form.css">
	<!--    <script src="script.js"></script>-->
</head>
<body>

<br>
<div class align="center">
	<a href="index.php">Accueil</a><br><br>
	<a href="login.php">Se connecter</a>
</div>

<?php
if (isset($user) && $user == true) {
    header('Location:login.php');
} else {
    if (isset($return) && !empty($return)) {
        echo $return;
    }
}
if (!empty($errors)) {
    foreach ($errors as $error){
        echo '<p>'.$error.'</p>';
    }
}
?>

<form class="box" action="register.php" method="post">
	<h1>Créer votre compte</h1>

    <input type="text" name="pseudo" id="pseudo" placeholder="Pseudonyme">

    <input type="text" name="email" id="email" placeholder="E-mail">

    <input type="password" name="mdp" id="mdp" placeholder="Mot de passe">

    <input type="password" name="mdp2" id="mdp2" placeholder="Confirmation mot de passe"><br>

     <input type="submit" name="" value="S'inscrire">
</form>

</body>
</html>
