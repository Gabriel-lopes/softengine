<?php
session_start();
/*$_SESSION['session'] = session_id();
var_dump($_SESSION);*/
require_once('functions.php');

if (!empty($_POST)) {
    $errors = array();
	$email = strip_tags($_POST['email']);
    $mdp = strip_tags($_POST['mdp']);


    if(empty($email)) {
        array_push($errors, 'E-mail manquant !');
    }

    if(empty($mdp)) {
        array_push($errors, 'Mot de passe manquant !');
    }
	if(count($errors) == 0){
        try {
            $log = login($email, $mdp);
        } 
		catch (Exception $e) {
            echo "problème avec la méthode login : ".$e->getMessage();
        }

        unset($email);
        unset($mdp);
    }
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Connexion</title>
	<link rel="stylesheet" href="css/style_form.css">
</head>

<body>
<br><br><br><br><br><br>
<div class align="center">
	<a href="index.php">Accueil</a><br><br>
	<a href="register.php">Créer un compte</a>
</div>
<?php

//Si il y'a des erreurs, il les affiches
if (!empty($errors)) {
	foreach ($errors as $error){
		echo '<p>'.$error.'</p>';
	}
}

//Si on se connecte en tant qu'administrateur on est automatiquement redirigé vers l'accueila page de modération
if (isset($_SESSION['role']) && $_SESSION['role'] == 1) {
    header('Location:admin/admin.php');
}

//Si on se connecte en tant qu'utilisateur lambda on est automatiquement redirigé vers l'accueil
if (isset($_SESSION['role']) && $_SESSION['role'] == 0) {
    echo '<br><br>'; ?>
	
	
	<!--Compte à rebours avant la redirection en Javascript-->
	<p>Vous allez être redirigés dans <span id="counter">8</span> seconde(s).</p>
	<script type="text/javascript">
	function countdown() {
		var i = document.getElementById('counter');
		if (parseInt(i.innerHTML)<=1) {
			location.href = 'index.php';
		}
		i.innerHTML = parseInt(i.innerHTML)-1;
	}
	setInterval(function(){ countdown(); },1000);
	</script>
	<!--Fin de la fonction en Javascript-->
	
	
	<?php
	echo '<br>';
    echo "<p>Vous êtes connecté(e) en tant que " .$_SESSION['pseudo']."</p>";
    echo '<p>Souhaitez-vous vous <a href=logout.php>déconnecter</a>?</p><br>';
}
 
if(!isset($_SESSION['id'])) {
?>
    
    <form class="box" action="login.php" method="post">
	
	<h1>Connexion</h1><br>
	
        <input type="text" name="email" id="email" placeholder="E-mail">

        <input type="password" name="mdp" id="mdp" placeholder="Mot de passe"><br>

        <input type="submit" name="" value="Se connecter">
    </form>
	
<?php } ?>

</body>
</html>
