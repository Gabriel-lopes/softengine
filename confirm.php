<?php 
session_start();
require_once('functions.php');

if (!empty($_POST)) {
    $errors = array();
	$mdp = strip_tags($_POST['mdp']);
    $cmdp = strip_tags($_POST['cmdp']);


    if(empty($mdp)) {
        array_push($errors, 'Indiquez votre mot de passe');
    }

    if(empty($cmdp)) {
        array_push($errors, 'Confirmez votre mot de passe');
    }
	
	if($mdp != $cmdp) {
		array_push($errors, 'Les mots de passe entrés ne sont pas identiques');
    }
	
	if(count($errors) == 0) {
        try {
            $log = delete($mdp, $cmdp);
        } 
		catch (Exception $e) {
            echo "problème avec la méthode delete : ".$e->getMessage();
        }

        unset($mdp);
        unset($cmdp);
    }
}


?>

<!DOCTYPE html>

<head>
	<meta charset="utf-8">
	<title>Suppression</title>
	<link rel="stylesheet" href="css/style_form.css">
</head>

<body>
	<pre>   <a href="profil.php">Retour</a>     <a href="index.php">Accueil</a> </pre>
	<br><br>
	
	<?php
	if (isset($_SESSION['del'])) {
    header('Location:delete.php');
	}
	
	if (!empty($errors)) {
		foreach ($errors as $error){
			echo '<p>'.$error.'</p>';
		}
	}
	?>
	
	<div align="center">
	<br>
	<?php echo "<p>Compte de ".$_SESSION['pseudo']."</p>"; ?>
	<br><br>
	<form class="box" action="confirm.php" method="post">
	
		<h1>Confirmez votre identité</h1><br>

        <input type="password" name="mdp" id="mdp" placeholder="Mot de passe">
		<input type="password" name="cmdp" id="cmdp" placeholder="Confirmer le mot de passe">
		<br>
        <input type="submit" name="" value="Supprimer">
    </form>
	</div>
	
</body>

</html>